const { mergeAll } = require('ramda')
const moment = require('moment')

module.exports = (provider, cache) => {

    return {

        async fetchExchangeRates() {

            const retrived = cache.get('fetchExchangeRates')

            const calculateTTL = () => {
                const now = moment().utcOffset(0)
                const refresh = moment().utcOffset(0)
                    .set({ hour: 0, minutes: 0 })

                return now.diff(refresh, 'seconds')
            }

            if (!retrived) {
                const response = await provider.fetchExchangeRates()
                cache.set('fetchExchangeRates', response, { ttl: calculateTTL() })
                return response

            }

            return retrived

        },

        getExchangeRates() {

            const parseExchangeData = rates =>
                rates.map(rate => ({ [rate.code]: rate }))
                    .reduce((prev, curr) => ({ ...prev, ...curr }), {})

            const fixed = {
                'PLN': { // eslint-disable-line quote-props
                    currency: 'złoty polski',
                    code: 'PLN',
                    bid: 1,
                    ask: 1
                }
            }

            return this.fetchExchangeRates()
                .then(([ data ]) => data.rates || Promise.reject(new Error('Invalid response')))
                .then(parseExchangeData)
                .then(rates => ({ ...rates, ...fixed }))

        },

        exchange(price, ...codes) {

            const calculatePrice = currencies =>
                rates => currencies.map(c => ({
                    [c]: rates[c] ? rates[c].ask * price : null
                }))

            return this.getExchangeRates()
                .then(calculatePrice(codes))
                .then(mergeAll)

        }

    }
}
