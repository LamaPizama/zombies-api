const { compose } = require('ramda')
const NodeCache = require('node-cache')
const cantorProvider = require('./providers/cantor.provider')
const exchangesService = require('./exchanges.service')

module.exports = compose(exchangesService)(cantorProvider, new NodeCache())

