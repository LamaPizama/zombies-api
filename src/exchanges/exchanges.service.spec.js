const NodeCache = require('node-cache')
const catorProvider = require('./providers/cantor.provider')
const exchangesService = require('./exchanges.service')

jest.mock('node-cache')
jest.mock('./providers/cantor.provider')

const exchanges = [
    {
        table: 'C',
        no: '200/C/NBP/2019',
        tradingDate: '2019-10-14',
        effectiveDate: '2019-10-15',
        rates: [
            {
                currency: 'dolar amerykański',
                code: 'USD',
                bid: 3.8562,
                ask: 3.9342
            }
        ]
    }
]

const parsed = {
    'USD': { // eslint-disable-line quote-props
        currency: 'dolar amerykański',
        code: 'USD',
        bid: 3.8562,
        ask: 3.9342
    },
    'PLN': { // eslint-disable-line quote-props
        currency: 'złoty polski',
        code: 'PLN',
        bid: 1,
        ask: 1
    }
}

describe('ExchangesService', () => {

    let cache
    let service

    beforeAll(() => {

        cache = new NodeCache()
        service = exchangesService(catorProvider, cache)

    })

    afterEach(() => {

        jest.clearAllMocks()

    })

    describe('ExchangeRates', () => {

        describe('fetchExchangeRates', () => {

            let get
            let set
            let fetchExchangeRates

            beforeAll(() => {

                get = jest.spyOn(cache, 'get')
                set = jest.spyOn(cache, 'set')
                fetchExchangeRates = jest.spyOn(catorProvider, 'fetchExchangeRates')

            })

            it('Should fetch exchanges rates and cache response if cache is empty', async () => {

                get.mockReturnValue(null)
                fetchExchangeRates
                    .mockImplementation(() => Promise.resolve(exchanges))

                expect(await service.fetchExchangeRates()).toBe(exchanges)
                expect(get).toHaveBeenCalled()
                expect(fetchExchangeRates).toHaveBeenCalled()
                expect(set).toHaveBeenCalledWith('fetchExchangeRates', exchanges, { ttl: expect.any(Number) })

            })

            it('Should return cached response', async () => {

                get.mockReturnValue(exchanges)
                fetchExchangeRates
                    .mockImplementation(() => Promise.resolve(exchanges))

                expect(await service.fetchExchangeRates()).toBe(exchanges)
                expect(get).toHaveBeenCalled()
                expect(fetchExchangeRates).toHaveBeenCalledTimes(0)
                expect(set).toHaveBeenCalledTimes(0)

            })

            it('Should reject with error', () => {

                get.mockReturnValue(null)
                fetchExchangeRates
                    .mockImplementation(() => Promise.reject(new Error()))

                expect(service.fetchExchangeRates()).rejects.toStrictEqual(expect.any(Error))
                expect(get).toHaveBeenCalled()
                expect(set).toHaveBeenCalledTimes(0)

            })

        })

        describe('getExchangeRates', () => {

            let fetchExchangeRates

            beforeAll(() => {

                fetchExchangeRates = jest.spyOn(service, 'fetchExchangeRates')

            })

            it('Should parse response', async () => {

                fetchExchangeRates
                    .mockImplementation(() => Promise.resolve(exchanges))

                expect(await service.getExchangeRates()).toStrictEqual(parsed)

            })

            it('Should reject with error if response is invalid', () => {

                fetchExchangeRates
                    .mockImplementation(() => Promise.resolve([]))

                expect(service.getExchangeRates()).rejects.toStrictEqual(expect.any(Error))

            })

        })

        describe('exchange', () => {

            let getExchangeRates

            beforeAll(() => {

                getExchangeRates = jest.spyOn(service, 'getExchangeRates')

            })

            it('Should exchange given ammount to currecy with given code', async () => {

                getExchangeRates
                    .mockImplementation(() => Promise.resolve(parsed))

                expect(await service.exchange(1, 'USD'))
                    .toStrictEqual({ 'USD': 1 * parsed['USD'].ask }) // eslint-disable-line

            })

            it('Should assign null if given currecy code does not exsist', async () => {

                getExchangeRates
                    .mockImplementation(() => Promise.resolve(parsed))

                expect(await service.exchange(1, 'USD', 'EUR'))
                    .toStrictEqual({ 'USD': 1 * parsed['USD'].ask, 'EUR': null }) // eslint-disable-line

            })

        })

    })

})
