const request = require('request-promise')

module.exports = {

    fetchExchangeRates: () => {

        return request.get({
            uri: 'http://api.nbp.pl/api/exchangerates/tables/C/today?format=json',
            json: true
        })

    }

}
