const mongoose = require('mongoose')
const logger = require('./common/logger/simple.logger')
const config = require('./common/config')
const app = require('./common/server')

const mongoUri = `${config.mongo.host}:${config.mongo.port}/${config.mongo.database}`

mongoose.connection.openUri(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
})

mongoose.connection.once('open', () => {
    logger.info(`Connected to database: ${mongoUri}`)
})

mongoose.connection.on('error', () => {
    throw new Error(`Unable to connect to database: ${mongoUri}`)
})

app.listen(config.port, () => {
    logger.info(`Server started on port ${config.port} (${config.env})`)
    logger.silly('Press CTRL-C to stop')
})

module.exports = app
