const express = require('express')
const zombiesRoutes = require('./zombies/routes/zombies.route')
const itemsRoutes = require('./items/routes/items.route') // eslint-disable-line
const router = express.Router() // eslint-disable-line new-cap

router.use('/zombies', zombiesRoutes)
router.use('/items', itemsRoutes)

module.exports = router
