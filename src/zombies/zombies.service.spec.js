const Zombie = require('./schemas/zombie.schema')
const zombiesService = require('./zombies.service')

jest.mock('./schemas/zombie.schema')

const zombie = {
    name: 'John',
    items: [
        '5d9fd1faf7e0322b34d44049',
        '5d9fd1fbf7e0322b34d4404a',
        '5d9fd1faf7e0322b34d44048'
    ]
}

describe('# ZombiesService', () => {

    let service

    beforeAll(() => {

        service = zombiesService(Zombie)

    })

    afterEach(() => {

        jest.clearAllMocks()

    })

    describe('create', () => {

        let create

        beforeAll(() => {

            create = jest.spyOn(Zombie, 'create')

        })

        it('Should create new zombie', async () => {

            create
                .mockImplementation(() => Promise.resolve(zombie))

            expect(await service.create(zombie)).toBe(zombie)
            expect(create).toHaveBeenCalledWith(zombie)

        })

        it('Should reject on error', () => {

            create
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.create(zombie)).rejects.toStrictEqual(expect.any(Error))
            expect(create).toHaveBeenCalledWith(zombie)

        })

    })

    describe('findById', () => {

        let findById

        beforeAll(() => {

            findById = jest.spyOn(Zombie, 'findById')

        })

        it('Should find zombie by id', async () => {

            findById
                .mockImplementation(() => Promise.resolve(zombie))

            expect(await service.findById('0')).toBe(zombie)
            expect(findById).toHaveBeenCalledWith('0')

        })

        it('Should return null if zombie with given id does not exist', async () => {

            findById
                .mockImplementation(() => Promise.resolve(null))

            expect(await service.findById()).toBe(null)

        })

        it('Should reject on error', () => {

            findById
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.findById()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('findByIdWithItems', () => {

        let findById

        beforeAll(() => {

            findById = jest.spyOn(Zombie, 'findById')

        })

        it('Should find zombie by id and resolve its items', async () => {

            const result = Promise.resolve(zombie)
            result.populate = jest.fn().mockReturnThis()

            findById.mockReturnValue(result)

            expect(await service.findByIdWithItems()).toBe(zombie)
            expect(result.populate).toHaveBeenCalledWith('items')

        })

        it('Should return null if zombie with given id does not exist', async () => {

            const result = Promise.resolve(null)
            result.populate = jest.fn().mockReturnThis()

            findById.mockReturnValue(result)

            expect(await service.findByIdWithItems()).toBe(null)

        })

        it('Should reject on error', () => {

            const result = Promise.reject(new Error())
            result.populate = jest.fn().mockReturnThis()

            findById.mockReturnValue(result)

            expect(service.findById()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('findByIdWithSumPrice', () => {

        let aggregate

        beforeAll(() => {

            aggregate = jest.spyOn(Zombie, 'aggregate')

        })

        it('Should find zombie by id with resolved items and calculate sum price', async () => {

            aggregate
                .mockImplementation(() => Promise.resolve([ { ...zombie, sumPrice: 1 } ]))

            expect(await service.findByIdWithSumPrice()).toMatchObject({
                ...zombie,
                sumPrice: 1
            })

        })

        it('Should return null if zombie with given id does not exsist', async () => {

            aggregate
                .mockImplementation(() => Promise.resolve([]))

            expect(await service.findByIdWithSumPrice()).toBe(null)

        })

        it('Should reject on error', () => {

            aggregate
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.findByIdWithSumPrice()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('find', () => {

        let find

        beforeAll(() => {

            find = jest.spyOn(Zombie, 'find')

        })

        it('Should return array of zombies', async () => {

            const result = Promise.resolve([ zombie ])
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(await service.find({ name: zombie.name })).toStrictEqual([ zombie ])
            expect(find).toHaveBeenCalledWith({ name: zombie.name })
            expect(result.skip).toHaveBeenCalledWith(0)
            expect(result.limit).toHaveBeenCalledWith(50)

        })

        it('Should return empty array if non of the zombies matches query', async () => {

            const result = Promise.resolve([])
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(await service.find({ name: zombie.name })).toStrictEqual([])
            expect(find).toHaveBeenCalledWith({ name: zombie.name })

        })

        it('Should reject on error', () => {

            const result = Promise.reject(new Error())
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(service.find()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('findWithItems', () => {

        let find

        beforeAll(() => {

            find = jest.spyOn(Zombie, 'find')

        })

        it('Should return array of zombies with resolved items', async () => {

            const result = Promise.resolve([ zombie ])
            result.populate = jest.fn().mockReturnThis()
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(await service.findWithItems({ name: zombie.name })).toStrictEqual([ zombie ])
            expect(find).toHaveBeenCalledWith({ name: zombie.name })
            expect(result.populate).toHaveBeenCalledWith('items')
            expect(result.skip).toHaveBeenCalledWith(0)
            expect(result.limit).toHaveBeenCalledWith(50)

        })

        it('Should return empty array if non of the zombies matches query', async () => {

            const result = Promise.resolve([])
            result.populate = jest.fn().mockReturnThis()
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(await service.findWithItems({ name: zombie.name })).toStrictEqual([])
            expect(find).toHaveBeenCalledWith({ name: zombie.name })

        })

        it('Should reject on error', () => {

            const result = Promise.reject(new Error())
            result.populate = jest.fn().mockReturnThis()
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(service.findWithItems()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('findWithSumPrice', () => {

        let aggregate

        beforeAll(() => {

            aggregate = jest.spyOn(Zombie, 'aggregate')

        })

        it('Should return zombie array with resolved items and calculated sum price', async () => {

            aggregate
                .mockImplementation(() => Promise.resolve([ { ...zombie, sumPrice: 1 } ]))

            expect(await service.findWithSumPrice()).toMatchObject([
                {
                    ...zombie,
                    sumPrice: 1
                }
            ])

        })

        it('Should return empty array if non of the zombies matches query', async () => {

            aggregate
                .mockImplementation(() => Promise.resolve([]))

            expect(await service.findWithSumPrice()).toStrictEqual([])

        })

        it('Should reject on error', () => {

            aggregate
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.findWithSumPrice()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('updateOne', () => {

        let findByIdAndUpdate

        beforeAll(() => {

            findByIdAndUpdate = jest.spyOn(Zombie, 'findByIdAndUpdate')

        })

        it('Should update zombie with given id', async () => {

            findByIdAndUpdate
                .mockImplementation(() => Promise.resolve(zombie))

            expect(await service.updateOne('0', zombie)).toBe(zombie)
            expect(findByIdAndUpdate).toHaveBeenCalledWith('0', zombie, { new: true, runValidators: true })

        })

        it('Should return null if zombie with given id does not exist', async () => {

            findByIdAndUpdate
                .mockImplementation(() => Promise.resolve(null))

            expect(await service.updateOne()).toBe(null)

        })

        it('Should reject on error', () => {

            findByIdAndUpdate
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.updateOne()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('updateMany', () => {

        let updateMany

        beforeAll(() => {

            updateMany = jest.spyOn(Zombie, 'updateMany')

        })

        it('Should update zombies matching query', async () => {

            const result = { number: 1, status: 'success' }

            updateMany
                .mockImplementation(() => Promise.resolve({ nModified: 1 }))

            expect(await service.updateMany({ name: zombie.name }, zombie)).toMatchObject(result)
            expect(updateMany).toHaveBeenCalledWith({ name: zombie.name }, zombie, { runValidators: true })

        })

        it('Should return error if non of the zombies matches query', async () => {

            const result = { number: 0, status: 'failure' }

            updateMany
                .mockImplementation(() => Promise.resolve({ nModified: 0 }))

            expect(await service.updateMany({ name: zombie.name }, zombie)).toMatchObject(result)

        })

        it('Should reject on error', () => {

            updateMany
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.updateMany()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('deleteOne', () => {

        let findByIdAndDelete

        beforeAll(() => {

            findByIdAndDelete = jest.spyOn(Zombie, 'findByIdAndDelete')

        })

        it('Should delete zombie with given id', async () => {

            findByIdAndDelete
                .mockImplementation(() => Promise.resolve(zombie))

            expect(await service.deleteOne('0')).toBe(zombie)
            expect(findByIdAndDelete).toHaveBeenCalledWith('0')

        })

        it('Should return null if zombie with given id does not exist', async () => {

            findByIdAndDelete
                .mockImplementation(() => Promise.resolve(null))

            expect(await service.deleteOne()).toBe(null)

        })

        it('Should reject on error', () => {

            findByIdAndDelete
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.deleteOne()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('deleteMany', () => {

        let deleteMany

        beforeAll(() => {

            deleteMany = jest.spyOn(Zombie, 'deleteMany')

        })

        it('Should delete zombies matching query', async () => {

            const result = { number: 1, status: 'success' }
            const spy = deleteMany
                .mockImplementation(() => Promise.resolve({ deletedCount: 1 }))

            expect(await service.deleteMany({ name: zombie.name })).toMatchObject(result)
            expect(spy).toHaveBeenCalledWith({ name: zombie.name })

        })

        it('Should return error if non of the zombies matches query', async () => {

            const result = { number: 0, status: 'failure' }

            deleteMany
                .mockImplementation(() => Promise.resolve({ deletedCount: 0 }))

            expect(await service.deleteMany({ name: zombie.name })).toMatchObject(result)

        })

        it('Should reject on error', () => {

            deleteMany
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.deleteMany()).rejects.toStrictEqual(expect.any(Error))

        })

    })

})
