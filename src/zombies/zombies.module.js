const { compose, curry } = require('ramda')
const Zombie = require('./schemas/zombie.schema')
const { exchangesService } = require('../exchanges')
const zombiesService = require('./zombies.service')
const zombiesCtrl = require('./zombies.controller')

module.exports = compose(curry(zombiesCtrl), zombiesService)(Zombie)(exchangesService)
