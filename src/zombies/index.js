const Zombie = require('./schemas/zombie.schema')
const zombieService = require('./zombies.service')
const zombiesController = require('./zombies.module')

module.exports = {
    zombiesController,
    itemsService: zombieService(Zombie),
    Zombie
}
