const { APIException } = require('../common/exceptions')

module.exports = (
    zombiesServcie,
    exchangesService
) => {

    return {

        create(req, res, next) {

            return zombiesServcie.create(req.body)
                .then(model => res.status(201).json(model))
                .catch(error => next(error))

        },

        findOne(req, res, next) {

            return zombiesServcie.findById(req.params.id)
                .then(zombie => zombie || Promise.reject(new APIException('Not found', 404)))
                .then(zombie => res.json(zombie))
                .catch(error => next(error))

        },

        findOneItems(req, res, next) {

            return zombiesServcie.findByIdWithItems(req.params.id)
                .then(zombie => zombie || Promise.reject(new APIException('Not found', 404)))
                .then(zombie => res.json(zombie))
                .catch(error => next(error))

        },

        findOneExchanges(req, res, next) {

            return zombiesServcie.findByIdWithSumPrice(req.params.id)
                .then(zombie => zombie || Promise.reject(new APIException('Not found', 404)))
                .then(zombie => this.calculateExchanges(zombie, 'USD', 'EUR', 'PLN'))
                .then(zombie => res.json(zombie))
                .catch(error => next(error))

        },

        find(req, res, next) {

            return zombiesServcie.find(req.query)
                .then(zombies => res.json(zombies))
                .catch(error => next(error))

        },

        findItems(req, res, next) {

            return zombiesServcie.findWithItems(req.query)
                .then(zombies => res.json(zombies))
                .catch(error => next(error))

        },

        findExchanges(req, res, next) {

            return zombiesServcie.findWithSumPrice(req.query)
                .then(zombies => Promise.all(
                    zombies.map(zombie => this.calculateExchanges(zombie, 'PLN', 'EUR', 'USD'))
                ))
                .then(zombies => res.json(zombies))
                .catch(error => next(error))

        },

        updateOne(req, res, next) {

            return zombiesServcie.updateOne(req.params.id, req.body)
                .then(zombie => zombie || Promise.reject(new APIException('Not found', 404)))
                .then(zombie => res.json(zombie))
                .catch(error => next(error))

        },

        updateMany(req, res, next) {

            return zombiesServcie.updateMany(req.query, req.body)
                .then(zombie => res.status(zombie.number ? 200 : 400).json(zombie))
                .catch(error => next(error))

        },

        deleteOne(req, res, next) {

            return zombiesServcie.deleteOne(req.params.id)
                .then(zombie => zombie || Promise.reject(new APIException('Not found', 404)))
                .then(zombie => res.json(zombie))
                .catch(error => next(error))

        },

        deleteMany(req, res, next) {

            return zombiesServcie.deleteMany(req.query)
                .then(zombie => res.status(zombie.number ? 200 : 400).json(zombie))
                .catch(error => next(error))

        },

        calculateExchanges(zombie, ...codes) {

            return exchangesService.exchange(zombie.sumPrice, ...codes)
                .then(exchange => ({ ...zombie, sumPrice: exchange }))

        }

    }
}
