const express = require('express')
const validate = require('express-validation')
const validation = require('../validation/zombies.validation')  // eslint-disable-line
const router = express.Router() // eslint-disable-line new-cap
const zombies = require('../zombies.module')

router.route('/')
    .get(validate(validation.find), zombies.find)
    .post(validate(validation.create), zombies.create.bind(zombies))
    .put(validate(validation.updateMany), zombies.updateMany.bind(zombies))
    .delete(validate(validation.deleteMany), zombies.deleteMany.bind(zombies))

router.route('/items')
    .get(zombies.findItems.bind(zombies))

router.route('/items/exchanges')
    .get(zombies.findExchanges.bind(zombies))

router.route('/:id')
    .get(zombies.findOne.bind(zombies))
    .put(validate(validation.updateOne), zombies.updateOne.bind(zombies))
    .delete(zombies.deleteOne.bind(zombies))

router.route('/:id/items')
    .get(zombies.findOneItems.bind(zombies))

router.route('/:id/items/exchanges')
    .get(zombies.findOneExchanges.bind(zombies))

module.exports = router
