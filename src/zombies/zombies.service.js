const mongoose = require('mongoose')

module.exports = zombiesModel => {

    return {

        create(zombie) {

            return zombiesModel.create(zombie)

        },

        findById(id) {

            return zombiesModel.findById(id)

        },

        findByIdWithItems(id) {

            return zombiesModel.findById(id)
                .populate('items')

        },

        findByIdWithSumPrice(id) {

            return zombiesModel.aggregate([
                {
                    $match: { _id: mongoose.Types.ObjectId(id) }
                },
                {
                    $lookup: {
                        from: 'items',
                        localField: 'items',
                        foreignField: '_id',
                        as: 'items'
                    }
                },
                {
                    $project: {
                        name: '$name',
                        items: '$items',
                        createdAt: '$createdAt',
                        updatedAt: '$updatedAt',
                        sumPrice: {
                            $reduce: {
                                input: '$items',
                                initialValue: 0,
                                in: { $add: [ '$$value', '$$this.price' ] }
                            }
                        }
                    }
                }
            ])
                .then(([ zombie ]) => zombie || null)

        },

        find({ limit = 50, skip = 0, ...query } = {}) {

            return zombiesModel.find(query)
                .skip(Number(skip))
                .limit(Number(limit))

        },

        findWithItems({ limit = 50, skip = 0, ...query } = {}) {

            return zombiesModel.find(query)
                .populate('items')
                .skip(Number(skip))
                .limit(Number(limit))

        },

        findWithSumPrice({ limit = 50, skip = 0, ...query } = {}) {

            return zombiesModel.aggregate([
                {
                    $match: query
                },
                {
                    $lookup: {
                        from: 'items',
                        localField: 'items',
                        foreignField: '_id',
                        as: 'items'
                    }
                },
                {
                    $skip: skip
                },
                {
                    $limit: limit
                },
                {
                    $project: {
                        name: '$name',
                        items: '$items',
                        createdAt: '$createdAt',
                        updatedAt: '$updatedAt',
                        sumPrice: {
                            $reduce: {
                                input: '$items',
                                initialValue: 0,
                                in: { $add: [ '$$value', '$$this.price' ] }
                            }
                        }
                    }
                }
            ])

        },

        updateOne(id, zombie) {

            return zombiesModel.findByIdAndUpdate(id, zombie, { new: true, runValidators: true })

        },

        updateMany(query, zombie) {

            return zombiesModel.updateMany(query, zombie, { runValidators: true })
                .then(({ nModified }) => ({
                    number: nModified,
                    status: nModified ? 'success' : 'failure'
                }))

        },

        deleteOne(id) {

            return zombiesModel.findByIdAndDelete(id)

        },

        deleteMany(query) {

            return zombiesModel.deleteMany(query)
                .then(({ deletedCount }) => ({
                    number: deletedCount,
                    status: deletedCount ? 'success' : 'failure'
                }))

        }

    }

}
