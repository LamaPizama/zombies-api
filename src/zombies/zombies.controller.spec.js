const { exchangesService } = require('../exchanges')
const zombiesService = require('./zombies.service')
const zombiesController = require('./zombies.controller')

const zombie = {
    name: 'John',
    items: [
        '5d9fd1faf7e0322b34d44049',
        '5d9fd1fbf7e0322b34d4404a',
        '5d9fd1faf7e0322b34d44048'
    ]
}

const res = {
    json: jest.fn(),
    status: jest.fn().mockReturnThis()
}

const next = jest.fn()

describe('# ZombiesController', () => {

    let service
    let controller

    beforeAll(() => {

        service = zombiesService()
        controller = zombiesController(service, exchangesService)

    })

    afterEach(() => {

        jest.clearAllMocks()

    })

    describe('calculateExchanges', () => {

        let exchange

        beforeAll(() => {

            exchange = jest.spyOn(exchangesService, 'exchange')

        })

        it('Should calaculate exchanges for given codes', async () => {

            exchange
                .mockImplementation(() => Promise.resolve({ USD: 1 }))

            expect(await controller.calculateExchanges({ ...zombie, sumPrice: 1 }, 'USD'))
                .toMatchObject({ ...zombie, sumPrice: { USD: 1 } })

            expect(exchange).toHaveBeenCalledWith(1, 'USD')

        })

        it('Should reject on error', () => {

            exchange
                .mockImplementation(() => Promise.reject(new Error()))

            expect(controller.calculateExchanges({ ...zombie, sumPrice: 1 }, 'USD'))
                .rejects
                .toStrictEqual(expect.any(Error))

        })

    })

    describe('create', () => {

        let create

        beforeAll(() => {

            create = jest.spyOn(service, 'create')

        })

        it('Should create new zombie', async () => {

            const req = { body: zombie }
            create.mockImplementation(() => Promise.resolve(zombie))

            await controller.create(req, res, next)

            expect(create).toHaveBeenCalledWith(zombie)
            expect(res.json).toHaveBeenCalledWith(zombie)
            expect(res.status).toHaveBeenCalledWith(201)

        })

        it('Should pass error to next', async () => {

            const req = { body: zombie }
            create.mockImplementation(() => Promise.reject(new Error()))

            await controller.create(req, res, next)

            expect(create).toHaveBeenCalledWith(zombie)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('findOne', () => {

        let findOne

        beforeAll(() => {

            findOne = jest.spyOn(service, 'findById')

        })

        it('Should find one zombie', async () => {

            const req = { params: { id: '0' } }
            findOne.mockImplementation(() => Promise.resolve(zombie))

            await controller.findOne(req, res, next)

            expect(findOne).toHaveBeenCalledWith(req.params.id)
            expect(res.json).toHaveBeenCalledWith(zombie)

        })

        it('Should return error if no zombie is found', async () => {

            const req = { params: { id: '0' } }
            findOne.mockImplementation(() => Promise.resolve(null))

            await controller.findOne(req, res, next)

            expect(findOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' } }
            findOne.mockImplementation(() => Promise.reject(new Error()))

            await controller.findOne(req, res, next)

            expect(findOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('findOneItems', () => {

        let findByIdWithItems

        beforeAll(() => {

            findByIdWithItems = jest.spyOn(service, 'findByIdWithItems')

        })

        it('Should find one zombie with resolved items', async () => {

            const req = { params: { id: '0' } }
            findByIdWithItems.mockImplementation(() => Promise.resolve(zombie))

            await controller.findOneItems(req, res, next)

            expect(findByIdWithItems).toHaveBeenCalledWith(req.params.id)
            expect(res.json).toHaveBeenCalledWith(zombie)

        })

        it('Should return error if no zombie is found', async () => {

            const req = { params: { id: '0' } }
            findByIdWithItems.mockImplementation(() => Promise.resolve(null))

            await controller.findOneItems(req, res, next)

            expect(findByIdWithItems).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' } }
            findByIdWithItems.mockImplementation(() => Promise.reject(new Error()))

            await controller.findOneItems(req, res, next)

            expect(findByIdWithItems).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('findOneExchanges', () => {

        let findByIdWithSumPrice
        let calculateExchanges

        beforeAll(() => {

            findByIdWithSumPrice = jest.spyOn(service, 'findByIdWithSumPrice')
            calculateExchanges = jest.spyOn(controller, 'calculateExchanges')

        })

        it('Should find one zombie and calculate exchanges of given currencies', async () => {

            const req = { params: { id: '0' } }
            findByIdWithSumPrice.mockImplementation(() => Promise.resolve(zombie))
            calculateExchanges.mockImplementation(zombie => Promise.resolve(zombie))

            await controller.findOneExchanges(req, res, next)

            expect(findByIdWithSumPrice).toHaveBeenCalledWith(req.params.id)
            expect(res.json).toHaveBeenCalledWith(zombie)
            expect(calculateExchanges).toHaveBeenCalled()

        })

        it('Should return error if no zombie is found', async () => {

            const req = { params: { id: '0' } }
            findByIdWithSumPrice.mockImplementation(() => Promise.resolve(null))

            await controller.findOneExchanges(req, res, next)

            expect(findByIdWithSumPrice).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))
            expect(calculateExchanges).toHaveBeenCalledTimes(0)

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' } }
            findByIdWithSumPrice.mockImplementation(() => Promise.reject(new Error()))

            await controller.findOneExchanges(req, res, next)

            expect(findByIdWithSumPrice).toHaveBeenCalledWith(req.params.id)
            expect(calculateExchanges).toHaveBeenCalledTimes(0)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('find', () => {

        let find

        beforeAll(() => {

            find = jest.spyOn(service, 'find')

        })

        it('Should return array of zombies with given parameters', async () => {

            const req = { query: { name: zombie.name } }
            find.mockImplementation(() => Promise.resolve([ zombie ]))

            await controller.find(req, res, next)

            expect(find).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith([ zombie ])

        })

        it('Should return empty array if non of the zombies matches query', async () => {

            const req = { query: { name: '0' } }
            find.mockImplementation(() => Promise.resolve([]))

            await controller.find(req, res, next)

            expect(find).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith([])

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: zombie.name } }
            find.mockImplementation(() => Promise.reject(new Error()))

            await controller.find(req, res, next)

            expect(find).toHaveBeenCalledWith(req.query)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('findItems', () => {

        let findWithItems

        beforeAll(() => {

            findWithItems = jest.spyOn(service, 'findWithItems')

        })

        it('Should return array of zombies with resolved items and given parameters', async () => {

            const req = { query: { name: zombie.name } }
            findWithItems.mockImplementation(() => Promise.resolve([ zombie ]))

            await controller.findItems(req, res, next)

            expect(findWithItems).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith([ zombie ])

        })

        it('Should return empty array if non of the zombies matches query', async () => {

            const req = { query: { name: '0' } }
            findWithItems.mockImplementation(() => Promise.resolve([]))

            await controller.findItems(req, res, next)

            expect(findWithItems).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith([])

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: zombie.name } }
            findWithItems.mockImplementation(() => Promise.reject(new Error()))

            await controller.findItems(req, res, next)

            expect(findWithItems).toHaveBeenCalledWith(req.query)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('findExchanges', () => {

        let findWithSumPrice
        let calculateExchanges

        beforeAll(() => {

            findWithSumPrice = jest.spyOn(service, 'findWithSumPrice')
            calculateExchanges = jest.spyOn(controller, 'calculateExchanges')

        })

        it('Should return array of zombies with resolved items and given parameters', async () => {

            const req = { query: { name: zombie.name } }
            findWithSumPrice.mockImplementation(() => Promise.resolve([ zombie ]))
            calculateExchanges.mockImplementation(zombie => Promise.resolve(zombie))

            await controller.findExchanges(req, res, next)

            expect(findWithSumPrice).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith([ zombie ])
            expect(calculateExchanges).toHaveBeenCalled()

        })

        it('Should return empty array if non of the zombies matches query', async () => {

            const req = { query: { name: '0' } }
            findWithSumPrice.mockImplementation(() => Promise.resolve([]))
            calculateExchanges.mockImplementation(zombie => Promise.resolve(zombie))

            await controller.findExchanges(req, res, next)

            expect(findWithSumPrice).toHaveBeenCalledWith(req.query)
            expect(calculateExchanges).toHaveBeenCalledTimes(0)
            expect(res.json).toHaveBeenCalledWith([])

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: zombie.name } }
            findWithSumPrice.mockImplementation(() => Promise.reject(new Error()))

            await controller.findExchanges(req, res, next)

            expect(findWithSumPrice).toHaveBeenCalledWith(req.query)
            expect(calculateExchanges).toHaveBeenCalledTimes(0)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('updateOne', () => {

        let updateOne

        beforeAll(() => {

            updateOne = jest.spyOn(service, 'updateOne')

        })

        it('Should update zombie with given id', async () => {

            const req = { params: { id: '0' }, body: zombie }
            updateOne.mockImplementation(() => Promise.resolve(zombie))

            await controller.updateOne(req, res, next)

            expect(updateOne).toHaveBeenCalledWith(req.params.id, zombie)
            expect(res.json).toHaveBeenCalledWith(zombie)

        })

        it('Should return error if no zombie is found', async () => {

            const req = { params: { id: '0' }, body: zombie }
            updateOne.mockImplementation(() => Promise.resolve(null))

            await controller.updateOne(req, res, next)

            expect(updateOne).toHaveBeenCalledWith(req.params.id, req.body)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' }, body: zombie }
            updateOne.mockImplementation(() => Promise.reject(new Error()))

            await controller.updateOne(req, res, next)

            expect(updateOne).toHaveBeenCalledWith(req.params.id, req.body)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('updateMany', () => {

        let updateMany

        beforeAll(() => {

            updateMany = jest.spyOn(service, 'updateMany')

        })

        it('Should update zombies matching query', async () => {

            const req = { query: { name: zombie.name }, body: zombie }
            const result = { number: 1, status: 'success' }

            updateMany.mockImplementation(() => Promise.resolve(result))

            await controller.updateMany(req, res, next)

            expect(updateMany).toHaveBeenCalledWith(req.query, zombie)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(200)

        })

        it('Should return error if non of the zombies matches query', async () => {

            const req = { query: { name: zombie.name }, body: zombie }
            const result = { number: 0, status: 'failure' }

            updateMany.mockImplementation(() => Promise.resolve(result))

            await controller.updateMany(req, res, next)

            expect(updateMany).toHaveBeenCalledWith(req.query, zombie)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(400)

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: zombie.name }, body: zombie }
            updateMany.mockImplementation(() => Promise.reject(new Error()))

            await controller.updateMany(req, res, next)

            expect(updateMany).toHaveBeenCalledWith(req.query, req.body)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('deleteOne', () => {

        let deleteOne

        beforeAll(() => {

            deleteOne = jest.spyOn(service, 'deleteOne')

        })

        it('Should delete zombie with given id', async () => {

            const req = { params: { id: '0' } }
            deleteOne.mockImplementation(() => Promise.resolve(zombie))

            await controller.deleteOne(req, res, next)

            expect(deleteOne).toHaveBeenCalledWith(req.params.id)
            expect(res.json).toHaveBeenCalledWith(zombie)

        })

        it('Should return error if no zombie is found', async () => {

            const req = { params: { id: '0' } }
            deleteOne.mockImplementation(() => Promise.resolve(null))

            await controller.deleteOne(req, res, next)

            expect(deleteOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' } }
            deleteOne.mockImplementation(() => Promise.reject(new Error()))

            await controller.deleteOne(req, res, next)

            expect(deleteOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('deleteMany', () => {

        let deleteMany

        beforeAll(() => {

            deleteMany = jest.spyOn(service, 'deleteMany')

        })

        it('Should update zombies matching query', async () => {

            const req = { query: { name: zombie.name } }
            const result = { number: 1, status: 'success' }

            deleteMany.mockImplementation(() => Promise.resolve(result))

            await controller.deleteMany(req, res, next)

            expect(deleteMany).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(200)

        })

        it('Should return error if non of the zombies matches query', async () => {

            const req = { query: { name: zombie.name } }
            const result = { number: 0, status: 'failure' }

            deleteMany.mockImplementation(() => Promise.resolve(result))

            await controller.deleteMany(req, res, next)

            expect(deleteMany).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(400)

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: zombie.name } }
            deleteMany.mockImplementation(() => Promise.reject(new Error()))

            await controller.deleteMany(req, res, next)

            expect(deleteMany).toHaveBeenCalledWith(req.query)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

})
