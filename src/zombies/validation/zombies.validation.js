const Joi = require('joi')

module.exports = {

    create: {

        body: Joi.object({
            name: Joi.string().required(),
            items: Joi.array().max(5).optional(),
        })

    },

    find: {

        query: Joi.object({
            limit: Joi.number().positive().optional(),
            skip: Joi.number().positive().optional()
        })

    },

    updateOne: {

        body: Joi.object({
            name: Joi.string().optional(),
            items: Joi.array().max(5).optional(),
        })

    },

    updateMany: {

        query: Joi.object().keys().min(1),

        body: Joi.object({
            name: Joi.string().required(),
            items: Joi.array().max(5).optional(),
        })

    },

    deleteMany: {

        query: Joi.object().keys().min(1)

    }

}

