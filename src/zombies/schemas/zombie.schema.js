const mongoose = require('mongoose')
const { compose, gte, length } = require('ramda')

const ZombieSchema = new mongoose.Schema({
    name: { type: String, required: true },
    items: {
        type: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Item'
            }
        ],
        validate: [ compose(gte(5), length), '{PATH} exceeds the limit of 5' ]
    }
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } })

module.exports = mongoose.model('Zombie', ZombieSchema)
