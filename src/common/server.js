const express = require('express')
const { ValidationError } = require('express-validation')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const compress = require('compression')
const cors = require('cors')
const APIException = require('./exceptions/api.exception')
const logger = require('./logger/http.logger')
const config = require('./config')
const { stackTrace, notFound, toException, winston } = require('./middlewares')
const routes = require('../routes') // eslint-disable-line
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(compress())
app.use(helmet())
app.use(cors())
app.use(winston(config.env, logger))

app.use('/', routes)

app.use((err, req, res, next) => { // eslint-disable-line

    if (err instanceof ValidationError) {
        const errors = err.errors.map(error => error.messages.join('. ')).join(' and ')
        return next(new APIException(errors, err.status, true))
    }

    return next(err)
})

app.use(toException())
app.use(notFound())
app.use(stackTrace(config.env === 'development'))

module.exports = app
