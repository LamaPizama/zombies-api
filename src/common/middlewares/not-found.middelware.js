const httpStatus = require('http-status')
const { APIException } = require('../exceptions')

module.exports = () =>
    (req, res, next) => next(new APIException('Not found', httpStatus.NOT_FOUND))
