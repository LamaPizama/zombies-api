const { APIException } = require('../exceptions')

module.exports = () =>
    (err, req, res, next) => {

        if (!(err instanceof APIException))
            return next(new APIException(err.message, err.status, err.isPublic))

        return next(err)

    }
