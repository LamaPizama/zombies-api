const httpStatus = require('http-status')

module.exports = (printStack = false) =>
    (err, req, res) =>
        res.status(err.status).json({
            message: err.isPublic
                ? err.message
                : httpStatus[err.status],
            stack: printStack
                ? err.stack
                : {}
        })
