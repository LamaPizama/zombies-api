const expressWinston = require('express-winston')

module.exports = (env, logger) => {
    return expressWinston.logger({
        winstonInstance: logger,
        msg: env === 'production'
            ? 'HTTP {{req.method}} {{req.url}} {{req.body}} {{res.statusCode}} {{res.responseTime}}ms'
            : 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
        colorStatus: true
    })
}
