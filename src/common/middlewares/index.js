const notFound = require('./not-found.middelware')
const stackTrace = require('./stack-trace.middleware')
const toException = require('./to-exeception.middleware')
const winston = require('./winston-logger.middleware')

module.exports = { stackTrace, notFound, toException, winston }
