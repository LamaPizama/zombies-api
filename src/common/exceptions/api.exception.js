const httpStatus = require('http-status')
const ExtendableExcetipon = require('./extendable.exception')

class APIExcetipon extends ExtendableExcetipon {

    constructor(message, status = httpStatus.INTERNAL_SERVER_ERROR, isPublic = false) {

        super(message, status, isPublic)

    }

}

module.exports = APIExcetipon
