class ExtendableExcetipion extends Error {

    constructor(message, status, isPublic) {

        super(message)
        this.name = this.constructor.name
        this.message = message
        this.status = status
        this.isPublic = isPublic
        Error.captureStackTrace(this, this.constructor.name)

    }

}

module.exports = ExtendableExcetipion
