const { createLogger, transports, format } = require('winston')
const config = require('../config')

module.exports = createLogger({

    transports: [

        new transports.Console({
            silent: config.env === 'test',
            format: format.printf(info => JSON.stringify(info, null, 4))
        })

    ]

})
