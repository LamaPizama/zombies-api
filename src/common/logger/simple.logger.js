const { createLogger, transports, format } = require('winston')
const config = require('../config')

module.exports = createLogger({

    transports: [

        new transports.Console({
            level: 'silly',
            silent: config.env === 'test',
            format: format.combine(
                format.colorize(),
                format.simple()
            )
        })

    ]

})
