const Item = require('./schemas/item.schema')
const itemsService = require('./items.service')

jest.mock('./schemas/item.schema')

const item = {
    itemId: 1,
    name: 'Trident',
    price: 200
}

describe('# ItemsService', () => {

    let service

    beforeAll(() => {

        service = itemsService(Item)

    })

    afterEach(() => {

        jest.clearAllMocks()

    })

    describe('create', () => {

        let create

        beforeAll(() => {

            create = jest.spyOn(Item, 'create')

        })

        it('Should create new item', async () => {

            create
                .mockImplementation(() => Promise.resolve(item))

            expect(await service.create(item)).toBe(item)
            expect(create).toHaveBeenCalledWith(item)

        })

        it('Should reject on error', () => {

            create
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.create(item)).rejects.toStrictEqual(expect.any(Error))
            expect(create).toHaveBeenCalledWith(item)

        })

    })

    describe('findById', () => {

        let findById

        beforeAll(() => {

            findById = jest.spyOn(Item, 'findById')

        })

        it('Should find item by id', async () => {

            findById
                .mockImplementation(() => Promise.resolve(item))

            expect(await service.findById('0')).toBe(item)
            expect(findById).toHaveBeenCalledWith('0')

        })

        it('Should return null if item with given id does not exist', async () => {

            findById
                .mockImplementation(() => Promise.resolve(null))

            expect(await service.findById()).toBe(null)

        })

        it('Should reject on error', () => {

            findById
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.findById()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('find', () => {

        let find

        beforeAll(() => {

            find = jest.spyOn(Item, 'find')

        })

        it('Should return array of items', async () => {

            const result = Promise.resolve([ item ])
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(await service.find({ name: item.name })).toStrictEqual([ item ])
            expect(find).toHaveBeenCalledWith({ name: item.name })
            expect(result.skip).toHaveBeenCalledWith(0)
            expect(result.limit).toHaveBeenCalledWith(50)

        })

        it('Should return empty array if non of the items matches query', async () => {

            const result = Promise.resolve([])
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(await service.find({ name: item.name })).toStrictEqual([])
            expect(find).toHaveBeenCalledWith({ name: item.name })

        })

        it('Should reject on error', () => {

            const result = Promise.reject(new Error())
            result.skip = jest.fn().mockReturnThis()
            result.limit = jest.fn().mockReturnThis()

            find.mockReturnValue(result)

            expect(service.find()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('updateOne', () => {

        let findByIdAndUpdate

        beforeAll(() => {

            findByIdAndUpdate = jest.spyOn(Item, 'findByIdAndUpdate')

        })

        it('Should update item with given id', async () => {

            findByIdAndUpdate
                .mockImplementation(() => Promise.resolve(item))

            expect(await service.updateOne('0', item)).toBe(item)
            expect(findByIdAndUpdate).toHaveBeenCalledWith('0', item, { new: true, runValidators: true })

        })

        it('Should return null if item with given id does not exist', async () => {

            findByIdAndUpdate
                .mockImplementation(() => Promise.resolve(null))

            expect(await service.updateOne()).toBe(null)

        })

        it('Should reject on error', () => {

            findByIdAndUpdate
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.updateOne()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('updateMany', () => {

        let updateMany

        beforeAll(() => {

            updateMany = jest.spyOn(Item, 'updateMany')

        })

        it('Should update items matching query', async () => {

            const result = { number: 1, status: 'success' }

            updateMany
                .mockImplementation(() => Promise.resolve({ nModified: 1 }))

            expect(await service.updateMany({ name: item.name }, item)).toMatchObject(result)
            expect(updateMany).toHaveBeenCalledWith({ name: item.name }, item, { runValidators: true })

        })

        it('Should return error if non of the items matches query', async () => {

            const result = { number: 0, status: 'failure' }

            updateMany
                .mockImplementation(() => Promise.resolve({ nModified: 0 }))

            expect(await service.updateMany({ name: item.name }, item)).toMatchObject(result)

        })

        it('Should reject on error', () => {

            updateMany
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.updateMany()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('deleteOne', () => {

        let findByIdAndDelete

        beforeAll(() => {

            findByIdAndDelete = jest.spyOn(Item, 'findByIdAndDelete')

        })

        it('Should delete item with given id', async () => {

            findByIdAndDelete
                .mockImplementation(() => Promise.resolve(item))

            expect(await service.deleteOne('0')).toBe(item)
            expect(findByIdAndDelete).toHaveBeenCalledWith('0')

        })

        it('Should return null if item with given id does not exist', async () => {

            findByIdAndDelete
                .mockImplementation(() => Promise.resolve(null))

            expect(await service.deleteOne()).toBe(null)

        })

        it('Should reject on error', () => {

            findByIdAndDelete
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.deleteOne()).rejects.toStrictEqual(expect.any(Error))

        })

    })

    describe('deleteMany', () => {

        let deleteMany

        beforeAll(() => {

            deleteMany = jest.spyOn(Item, 'deleteMany')

        })

        it('Should delete items matching query', async () => {

            const result = { number: 1, status: 'success' }
            const spy = deleteMany
                .mockImplementation(() => Promise.resolve({ deletedCount: 1 }))

            expect(await service.deleteMany({ name: item.name })).toMatchObject(result)
            expect(spy).toHaveBeenCalledWith({ name: item.name })

        })

        it('Should return error if non of the items matches query', async () => {

            const result = { number: 0, status: 'failure' }

            deleteMany
                .mockImplementation(() => Promise.resolve({ deletedCount: 0 }))

            expect(await service.deleteMany({ name: item.name })).toMatchObject(result)

        })

        it('Should reject on error', () => {

            deleteMany
                .mockImplementation(() => Promise.reject(new Error()))

            expect(service.deleteMany()).rejects.toStrictEqual(expect.any(Error))

        })

    })

})
