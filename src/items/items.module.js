const { compose } = require('ramda')
const Item = require('./schemas/item.schema')
const itemsService = require('./items.service')
const itemsCtrl = require('./items.controller')

module.exports = compose(itemsCtrl, itemsService)(Item)

