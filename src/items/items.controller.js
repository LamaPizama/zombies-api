const { APIException } = require('../common/exceptions')

module.exports = itemsService => ({

    create(req, res, next) {

        return itemsService.create(req.body)
            .then(item => res.status(201).json(item))
            .catch(error => next(error))

    },

    findOne(req, res, next) {

        return itemsService.findById(req.params.id)
            .then(item => item || Promise.reject(new APIException('Not found', 404)))
            .then(item => res.json(item))
            .catch(error => next(error))

    },

    find(req, res, next) {

        return itemsService.find(req.query)
            .then(item => res.json(item))
            .catch(error => next(error))

    },

    updateOne(req, res, next) {

        return itemsService.updateOne(req.params.id, req.body)
            .then(item => item || Promise.reject(new APIException('Not found', 404)))
            .then(item => res.json(item))
            .catch(error => next(error))

    },

    updateMany(req, res, next) {

        return itemsService.updateMany(req.query, req.body)
            .then(item => res.status(item.number ? 200 : 400).json(item))
            .catch(error => next(error))

    },

    deleteOne(req, res, next) {

        return itemsService.deleteOne(req.params.id)
            .then(item => item || Promise.reject(new APIException('Not found', 404)))
            .then(item => res.json(item))
            .catch(error => next(error))

    },

    deleteMany(req, res, next) {

        return itemsService.deleteMany(req.query)
            .then(item => res.status(item.number ? 200 : 400).json(item))
            .catch(error => next(error))

    }

})
