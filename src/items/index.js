const Item = require('./schemas/item.schema')
const itemsService = require('./items.service')
const itemsController = require('./items.module')

module.exports = {
    itemsController,
    itemsService: itemsService(Item),
    Item
}
