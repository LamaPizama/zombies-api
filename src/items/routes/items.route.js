const express = require('express')
const validate = require('express-validation')
const validation = require('../validation/items.validation') // eslint-disable-line
const router = express.Router() // eslint-disable-line new-cap
const items = require('../items.module')

router.route('/')
    .get(validate(validation.find), items.find.bind(items))
    .post(validate(validation.create), items.create.bind(items))
    .put(validate(validation.updateMany), items.updateMany.bind(items))
    .delete(validate(validation.deleteMany), items.deleteMany.bind(items))

router.route('/:id')
    .get(items.findOne.bind(items))
    .put(validate(validation.updateOne), items.updateOne.bind(items))
    .delete(items.deleteOne.bind(items))

module.exports = router
