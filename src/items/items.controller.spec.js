const itemsService = require('./items.service')
const itemsController = require('./items.controller')

const item = {
    itemId: 1,
    name: 'Trident',
    price: 200
}

const res = {
    json: jest.fn(),
    status: jest.fn().mockReturnThis()
}

const next = jest.fn()

describe('# ItemsController', () => {

    let service
    let controller

    beforeAll(() => {

        service = itemsService()
        controller = itemsController(service)

    })

    afterEach(() => {

        jest.clearAllMocks()

    })

    describe('create', () => {

        let create

        beforeAll(() => {

            create = jest.spyOn(service, 'create')

        })

        it('Should create new item', async () => {

            const req = { body: item }
            create.mockImplementation(() => Promise.resolve(item))

            await controller.create(req, res, next)

            expect(create).toHaveBeenCalledWith(item)
            expect(res.json).toHaveBeenCalledWith(item)
            expect(res.status).toHaveBeenCalledWith(201)

        })

        it('Should pass error to next', async () => {

            const req = { body: item }
            create.mockImplementation(() => Promise.reject(new Error()))

            await controller.create(req, res, next)

            expect(create).toHaveBeenCalledWith(item)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('findOne', () => {

        let findOne

        beforeAll(() => {

            findOne = jest.spyOn(service, 'findById')

        })

        it('Should find one item', async () => {

            const req = { params: { id: '0' } }
            findOne.mockImplementation(() => Promise.resolve(item))

            await controller.findOne(req, res, next)

            expect(findOne).toHaveBeenCalledWith(req.params.id)
            expect(res.json).toHaveBeenCalledWith(item)

        })

        it('Should return error if no item is found', async () => {

            const req = { params: { id: '0' } }
            findOne.mockImplementation(() => Promise.resolve(null))

            await controller.findOne(req, res, next)

            expect(findOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' } }
            findOne.mockImplementation(() => Promise.reject(new Error()))

            await controller.findOne(req, res, next)

            expect(findOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('find', () => {

        let find

        beforeAll(() => {

            find = jest.spyOn(service, 'find')

        })

        it('Should return array of items with given parameters', async () => {

            const req = { query: { name: item.name } }
            find.mockImplementation(() => Promise.resolve([ item ]))

            await controller.find(req, res, next)

            expect(find).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith([ item ])

        })

        it('Should return empty array if non of the items matches query', async () => {

            const req = { query: { name: '0' } }
            find.mockImplementation(() => Promise.resolve([]))

            await controller.find(req, res, next)

            expect(find).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith([])

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: item.name } }
            find.mockImplementation(() => Promise.reject(new Error()))

            await controller.find(req, res, next)

            expect(find).toHaveBeenCalledWith(req.query)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('updateOne', () => {

        let updateOne

        beforeAll(() => {

            updateOne = jest.spyOn(service, 'updateOne')

        })

        it('Should update item with given id', async () => {

            const req = { params: { id: '0' }, body: item }
            updateOne.mockImplementation(() => Promise.resolve(item))

            await controller.updateOne(req, res, next)

            expect(updateOne).toHaveBeenCalledWith(req.params.id, item)
            expect(res.json).toHaveBeenCalledWith(item)

        })

        it('Should return error if no item is found', async () => {

            const req = { params: { id: '0' }, body: item }
            updateOne.mockImplementation(() => Promise.resolve(null))

            await controller.updateOne(req, res, next)

            expect(updateOne).toHaveBeenCalledWith(req.params.id, req.body)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' }, body: item }
            updateOne.mockImplementation(() => Promise.reject(new Error()))

            await controller.updateOne(req, res, next)

            expect(updateOne).toHaveBeenCalledWith(req.params.id, req.body)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('updateMany', () => {

        let updateMany

        beforeAll(() => {

            updateMany = jest.spyOn(service, 'updateMany')

        })

        it('Should update items matching query', async () => {

            const req = { query: { name: item.name }, body: item }
            const result = { number: 1, status: 'success' }

            updateMany.mockImplementation(() => Promise.resolve(result))

            await controller.updateMany(req, res, next)

            expect(updateMany).toHaveBeenCalledWith(req.query, item)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(200)

        })

        it('Should return error if non of the items matches query', async () => {

            const req = { query: { name: item.name }, body: item }
            const result = { number: 0, status: 'failure' }

            updateMany.mockImplementation(() => Promise.resolve(result))

            await controller.updateMany(req, res, next)

            expect(updateMany).toHaveBeenCalledWith(req.query, item)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(400)

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: item.name }, body: item }
            updateMany.mockImplementation(() => Promise.reject(new Error()))

            await controller.updateMany(req, res, next)

            expect(updateMany).toHaveBeenCalledWith(req.query, req.body)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('deleteOne', () => {

        let deleteOne

        beforeAll(() => {

            deleteOne = jest.spyOn(service, 'deleteOne')

        })

        it('Should delete item with given id', async () => {

            const req = { params: { id: '0' } }
            deleteOne.mockImplementation(() => Promise.resolve(item))

            await controller.deleteOne(req, res, next)

            expect(deleteOne).toHaveBeenCalledWith(req.params.id)
            expect(res.json).toHaveBeenCalledWith(item)

        })

        it('Should return error if no item is found', async () => {

            const req = { params: { id: '0' } }
            deleteOne.mockImplementation(() => Promise.resolve(null))

            await controller.deleteOne(req, res, next)

            expect(deleteOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

        it('Should pass error to next', async () => {

            const req = { params: { id: '0' } }
            deleteOne.mockImplementation(() => Promise.reject(new Error()))

            await controller.deleteOne(req, res, next)

            expect(deleteOne).toHaveBeenCalledWith(req.params.id)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

    describe('deleteMany', () => {

        let deleteMany

        beforeAll(() => {

            deleteMany = jest.spyOn(service, 'deleteMany')

        })

        it('Should update items matching query', async () => {

            const req = { query: { name: item.name } }
            const result = { number: 1, status: 'success' }

            deleteMany.mockImplementation(() => Promise.resolve(result))

            await controller.deleteMany(req, res, next)

            expect(deleteMany).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(200)

        })

        it('Should return error if non of the items matches query', async () => {

            const req = { query: { name: item.name } }
            const result = { number: 0, status: 'failure' }

            deleteMany.mockImplementation(() => Promise.resolve(result))

            await controller.deleteMany(req, res, next)

            expect(deleteMany).toHaveBeenCalledWith(req.query)
            expect(res.json).toHaveBeenCalledWith(result)
            expect(res.status).toHaveBeenCalledWith(400)

        })

        it('Should pass error to next', async () => {

            const req = { query: { name: item.name } }
            deleteMany.mockImplementation(() => Promise.reject(new Error()))

            await controller.deleteMany(req, res, next)

            expect(deleteMany).toHaveBeenCalledWith(req.query)
            expect(next).toHaveBeenCalledWith(expect.any(Error))

        })

    })

})
