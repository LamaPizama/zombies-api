const Joi = require('joi')

module.exports = {

    find: {

        query: Joi.object({
            limit: Joi.number().positive().optional(),
            skip: Joi.number().positive().optional()
        })

    },

    create: {

        body: Joi.object({
            itemId: Joi.number().required(),
            name: Joi.string().required(),
            price: Joi.number().positive().required()
        })

    },

    updateOne: {

        body: Joi.object({
            itemId: Joi.number().optional(),
            name: Joi.string().optional(),
            price: Joi.number().positive().optional()
        })

    },

    updateMany: {

        query: Joi.object().keys().min(1),

        body: Joi.object({
            itemId: Joi.number().optional(),
            name: Joi.string().optional(),
            price: Joi.number().positive().optional()
        })

    },

    deleteMany: {

        query: Joi.object().keys().min(1)

    }

}
