
module.exports = itemsModel => {

    return {

        create(item) {

            return itemsModel.create(item)

        },

        findById(id) {

            return itemsModel.findById(id)

        },

        find({ limit = 50, skip = 0, ...query } = {}) {

            return itemsModel.find(query)
                .skip(Number(skip))
                .limit(Number(limit))

        },

        updateOne(id, item) {

            return itemsModel.findByIdAndUpdate(id, item, { new: true, runValidators: true })

        },

        updateMany(query, item) {

            return itemsModel.updateMany(query, item, { runValidators: true })
                .then(({ nModified }) => ({
                    number: nModified,
                    status: nModified ? 'success' : 'failure'
                }))

        },

        deleteOne(id) {

            return itemsModel.findByIdAndDelete(id)

        },

        deleteMany(query) {

            return itemsModel.deleteMany(query)
                .then(({ deletedCount }) => ({
                    number: deletedCount,
                    status: deletedCount ? 'success' : 'failure'
                }))

        }

    }

}
