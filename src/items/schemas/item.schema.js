const mongoose = require('mongoose')

const ItemSchema = new mongoose.Schema({
    itemId: { type: Number, required: true, unique: true },
    name: { type: String, required: true },
    price: { type: Number, required: true }
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } })

module.exports = mongoose.model('Item', ItemSchema)
