module.exports = {
    preset: '@shelf/jest-mongodb',
    moduleFileExtensions: [
        'js',
        'json'
    ],
    rootDir: '.',
    testRegex: 'e2e.spec.js$'
}
