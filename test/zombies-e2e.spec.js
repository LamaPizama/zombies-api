const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
const nock = require('nock')
const request = require('supertest')
const { Item } = require('../src/items')
const { Zombie } = require('../src/zombies')
const app = require('../src/common/server')

const item = {
    itemId: 1,
    name: 'Trident',
    price: 1
}

const zombie = {
    name: 'John',
    items: []
}

const exchanges = [
    {
        table: 'C',
        no: '200/C/NBP/2019',
        tradingDate: '2019-10-14',
        effectiveDate: '2019-10-15',
        rates: [
            {
                currency: 'dolar amerykański',
                code: 'USD',
                bid: 3.8562,
                ask: 3.9342
            },
            {
                currency: 'euro',
                code: 'EUR',
                bid: 4.4969,
                ask: 4.5142
            }
        ]
    }
]

describe('# ZombiesModule (e2e)', () => {

    let database

    beforeAll(async () => {

        database = new MongoMemoryServer()
        const mongoUri = await database.getConnectionString()
        await mongoose.connect(mongoUri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })

    })

    afterEach(async () => {

        await Item.deleteMany()
        await Zombie.deleteMany()

    })

    afterAll(async () => {

        await mongoose.disconnect()
        await database.stop()

    })

    describe('# POST /zombies', () => {

        it('Should return validation error if zombie is not valid', async () => {

            const response = await request(app)
                .post('/zombies')
                .send({})

            expect(response.status).toEqual(400)

        })

        it('Should create new zombie', async () => {

            const response = await request(app)
                .post('/zombies')
                .send(zombie)

            expect(response.status).toEqual(201)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...zombie,
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

    describe('# GET /zombies/:id', () => {

        it('Should return not found if zombie with given id does not exsist', async () => {

            const response = await request(app)
                .get('/zombies/5d9e362ce4a0a953b8990000')

            expect(response.status).toEqual(404)

        })

        it('Should return zombie with given id', async () => {

            const createdZombie = await Zombie.create(zombie)

            const response = await request(app)
                .get(`/zombies/${createdZombie._id}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...zombie,
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

    describe('# GET /zombies/:id/items', () => {

        it('Should return not found if zombie with given id does not exsist', async () => {

            const response = await request(app)
                .get('/zombies/5d9e362ce4a0a953b8990000/zombies')

            expect(response.status).toEqual(404)

        })

        it('Should return zombie with resolved items', async () => {

            const createdItem = await Item.create(item)
            const createdZombie = await Zombie.create({ ...zombie, items: [ createdItem._id ] })

            const response = await request(app)
                .get(`/zombies/${createdZombie._id}/items`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...zombie,
                items: [ expect.objectContaining(item) ],
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

    describe('# GET /zombies/:id/items/exchanges', () => {

        it('Should return not found if zombie with given id does not exsist', async () => {

            const response = await request(app)
                .get('/zombies/5d9e362ce4a0a953b8990000/items/exchanges')

            expect(response.status).toEqual(404)

        })

        it('Should return error if exchanges can not be fetched', async () => {

            const createdItem = await Item.create(item)
            const createdZombie = await Zombie.create({ ...zombie, items: [ createdItem._id ] })

            nock('http://api.nbp.pl/')
                .get(/api.*/)
                .reply(500)

            const response = await request(app)
                .get(`/zombies/${createdZombie._id}/items/exchanges`)

            expect(response.status).toEqual(500)

        })

        it('Should return zombie with resolved items and calculated exchanges', async () => {

            const createdItem = await Item.create(item)
            const createdZombie = await Zombie.create({ ...zombie, items: [ createdItem._id ] })

            nock('http://api.nbp.pl/')
                .get(/api.*/)
                .reply(200, exchanges)

            const response = await request(app)
                .get(`/zombies/${createdZombie._id}/items/exchanges`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...zombie,
                items: [ expect.objectContaining(item) ],
                sumPrice: {
                    USD: 3.9342,
                    EUR: 4.5142,
                    PLN: 1
                },
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

    describe('# GET /zombies', () => {

        it('Should return empty array if item with given parameters does not exsist', async () => {

            const response = await request(app)
                .get('/zombies?name=0')

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual([])

        })

        it('Should return list of zombies with given parameters', async () => {

            await Zombie.create(zombie)

            const response = await request(app)
                .get(`/zombies?name=${zombie.name}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject([
                {
                    _id: expect.any(String),
                    ...zombie,
                    createdAt: expect.any(String),
                    updatedAt: expect.any(String)
                }
            ])

        })

    })

    describe('# GET /zombies/items', () => {

        it('Should return empty array if zombie with given parameters does not exsist', async () => {

            const response = await request(app)
                .get('/zombies/items?name=0')

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual([])

        })

        it('Should return list of zombies with resolved items and given parameters', async () => {

            const createdItem = await Item.create(item)
            await Zombie.create({ ...zombie, items: [ createdItem._id ] })

            const response = await request(app)
                .get(`/zombies/items?name=${zombie.name}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject([
                {
                    _id: expect.any(String),
                    ...zombie,
                    items: [ expect.objectContaining(item) ],
                    createdAt: expect.any(String),
                    updatedAt: expect.any(String)
                }
            ])

        })

    })

    describe('# GET /zombies/items/exchanges', () => {

        it('Should return empty array if zombie with given parameters does not exsist', async () => {

            const response = await request(app)
                .get('/zombies/items/exchanges?name=0')

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual([])

        })

        it('Should return list of zombies with resolved items and exchanges', async () => {

            const createdItem = await Item.create(item)
            await Zombie.create({ ...zombie, items: [ createdItem._id ] })

            nock('http://api.nbp.pl/')
                .get(/api.*/)
                .reply(200, exchanges)

            const response = await request(app)
                .get(`/zombies/items/exchanges?name=${zombie.name}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject([
                {
                    _id: expect.any(String),
                    ...zombie,
                    items: [ expect.objectContaining(item) ],
                    sumPrice: {
                        USD: 3.9342,
                        EUR: 4.5142,
                        PLN: 1
                    },
                    createdAt: expect.any(String),
                    updatedAt: expect.any(String)
                }
            ])

        })

    })

    describe('# PUT /zombies', () => {

        it('Should return failure status', async () => {

            const response = await request(app)
                .put('/zombies?name=0')
                .send({ name: 'Joe' })

            expect(response.status).toEqual(400)
            expect(response.body).toStrictEqual({
                status: 'failure',
                number: 0
            })

        })

        it('Should update zombies with given parameters and return success status', async () => {

            await Zombie.create(zombie)

            const response = await request(app)
                .put(`/zombies?name=${zombie.name}`)
                .send({ name: 'Joe' })

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual({
                status: 'success',
                number: 1
            })

        })

    })

    describe('# PUT /zombies/:id', () => {

        it('Should return not found if item with given id does not exsist', async () => {

            const response = await request(app)
                .put('/zombies/5d9e362ce4a0a953b8990000')

            expect(response.status).toEqual(404)

        })

        it('Should update item with given id', async () => {

            const created = await Zombie.create(zombie)

            const response = await request(app)
                .put(`/zombies/${created._id}`)
                .send({ name: 'Joe' })

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...zombie,
                name: 'Joe',
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

    describe('# DELETE /zombies', () => {

        it('Should return failure status', async () => {

            const response = await request(app)
                .delete('/zombies?name=0')

            expect(response.status).toEqual(400)
            expect(response.body).toStrictEqual({
                status: 'failure',
                number: 0
            })

        })

        it('Should delete zombies with given parameters and return success status', async () => {

            await Zombie.create(zombie)

            const response = await request(app)
                .delete(`/zombies?name=${zombie.name}`)

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual({
                status: 'success',
                number: 1
            })

        })

    })

    describe('# DELETE /zombies/:id', () => {

        it('Should return not found if item with given id does not exsist', async () => {

            const response = await request(app)
                .delete('/zombies/5d9e362ce4a0a953b8990000')

            expect(response.status).toEqual(404)

        })

        it('Should delete item with given id', async () => {

            const created = await Zombie.create(zombie)

            const response = await request(app)
                .delete(`/zombies/${created._id}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...zombie,
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

})
