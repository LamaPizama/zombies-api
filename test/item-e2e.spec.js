const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
const request = require('supertest')
const { Item } = require('../src/items')
const app = require('../src/common/server')

const item = {
    itemId: 1,
    name: 'Trident',
    price: 200
}

describe('# ItemsModule (e2e)', () => {

    let database

    beforeAll(async () => {

        database = new MongoMemoryServer()
        const mongoUri = await database.getConnectionString()
        await mongoose.connect(mongoUri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })

    })

    afterEach(async () => {

        await Item.deleteMany()

    })

    afterAll(async () => {

        await mongoose.disconnect()
        await database.stop()

    })

    describe('# POST /items', () => {

        it('Should create new item', async () => {

            const response = await request(app)
                .post('/items')
                .send(item)

            expect(response.status).toEqual(201)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...item,
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

        it('Should return validation error if item is not valid', async () => {

            const response = await request(app)
                .post('/items')
                .send({})

            expect(response.status).toEqual(400)

        })

    })

    describe('# GET /items', () => {

        it('Should return empty array if item with given parameters does not exsist', async () => {

            const response = await request(app)
                .get('/items?name=0')

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual([])

        })

        it('Should return list of items with given parameters', async () => {

            await Item.create(item)

            const response = await request(app)
                .get(`/items?name=${item.name}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject([
                {
                    _id: expect.any(String),
                    ...item,
                    createdAt: expect.any(String),
                    updatedAt: expect.any(String)
                }
            ])

        })

    })

    describe('# GET /items/:id', () => {

        it('Should return items with given id', async () => {

            const created = await Item.create(item)

            const response = await request(app)
                .get(`/items/${created._id}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...item,
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

        it('Should return not found if item with given id does not exsist', async () => {

            const response = await request(app)
                .get('/items/5d9e362ce4a0a953b8990000')

            expect(response.status).toEqual(404)

        })

    })

    describe('# PUT /items', () => {

        it('Should return failure status', async () => {

            const response = await request(app)
                .put('/items?name=0')
                .send({ name: 'Sword' })

            expect(response.status).toEqual(400)
            expect(response.body).toStrictEqual({
                status: 'failure',
                number: 0
            })

        })

        it('Should update items with given parameters and return success status', async () => {

            await Item.create(item)

            const response = await request(app)
                .put(`/items?name=${item.name}`)
                .send({ name: 'Sword' })

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual({
                status: 'success',
                number: 1
            })

        })

    })

    describe('# PUT /items/:id', () => {

        it('Should return not found if item with given id does not exsist', async () => {

            const response = await request(app)
                .put('/items/5d9e362ce4a0a953b8990000')

            expect(response.status).toEqual(404)

        })

        it('Should update item with given id', async () => {

            const created = await Item.create(item)

            const response = await request(app)
                .put(`/items/${created._id}`)
                .send({ name: 'Sword' })

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...item,
                name: 'Sword',
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

    describe('# DELETE /items', () => {

        it('Should return failure status', async () => {

            const response = await request(app)
                .delete('/items?name=0')

            expect(response.status).toEqual(400)
            expect(response.body).toStrictEqual({
                status: 'failure',
                number: 0
            })

        })

        it('Should delete items with given parameters and return success status', async () => {

            await Item.create(item)

            const response = await request(app)
                .delete(`/items?name=${item.name}`)

            expect(response.status).toEqual(200)
            expect(response.body).toStrictEqual({
                status: 'success',
                number: 1
            })

        })

    })

    describe('# DELETE /items/:id', () => {

        it('Should return not found if item with given id does not exsist', async () => {

            const response = await request(app)
                .delete('/items/5d9e362ce4a0a953b8990000')

            expect(response.status).toEqual(404)

        })

        it('Should delete item with given id', async () => {

            const created = await Item.create(item)

            const response = await request(app)
                .delete(`/items/${created._id}`)

            expect(response.status).toEqual(200)
            expect(response.body).toMatchObject({
                _id: expect.any(String),
                ...item,
                createdAt: expect.any(String),
                updatedAt: expect.any(String)
            })

        })

    })

})
