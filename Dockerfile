
FROM node:12.11.1-alpine

WORKDIR /app

ADD package.json package-lock.json /app/

RUN npm ci

COPY . /app/

EXPOSE 3005

CMD [ "npm", "run", "start" ]
